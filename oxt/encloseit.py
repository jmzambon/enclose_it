# -_- coding: utf-8 -_-

'''
Implemented with the great help of Hanya: https://forum.openoffice.org/en/forum/viewtopic.php?p=200664&sid=1c5200cfe3561d4eae7850cda10300bf#p200664.
'''

import unohelper
import traceback
from string import whitespace, punctuation

from com.sun.star.awt import XKeyHandler
from com.sun.star.util import XChangesListener
from com.sun.star.lang import XInitialization, XServiceInfo
from com.sun.star.frame import XStatusListener, XDispatchProvider, XDispatch, FeatureStateEvent
from com.sun.star.beans import PropertyValue


IMPL_NAME = "ooo.jmz.enclose_it.impl"
NONWORDCHARS = set(whitespace + punctuation)
DEFAULTPAIRS = ("()", "[]", "{}", "''", '""')
PAIRS = {}
FRAMES = {}
OPTIONS = {}
cfg_access_registry = None
cfg_access_autocorrect = None


def updatepairs():
    PAIRS.clear()
    try:
        p = OPTIONS["charpairs"].replace(" ", "").strip(',')
        PAIRS.update(s for s in p.split(',') if len(s)==2)
    except (KeyError, ValueError):
        traceback.print_exc()
        PAIRS.update(DEFAULTPAIRS)


class ChangesListener(unohelper.Base, XChangesListener):
    def __init__(self, properties):
        self.props = properties

    def changesOccurred(self, event):
        changes = event.Changes
        for change in changes:
            if change.Accessor in self.props:
                OPTIONS[change.Accessor] = change.Element
                if change.Accessor == "charpairs":
                    updatepairs()

    def disposing(self, source):
        pass


class KeyHandler(unohelper.Base, XKeyHandler):
    def __init__(self, localedata, controller):
        self.localedata = localedata
        self.controller = controller

    # XKeyHandler
    def keyPressed(self, keyevent):
        try:
            ret = False
            k = keyevent.KeyChar.value
            if k in PAIRS:
                ret = self.enclose_it(k, PAIRS[k])
            return ret
        except Exception:
            traceback.print_exc()
            return False

    def keyReleased(self, keyevent):
        return False

    def disposing(self, source):
        pass

    # private
    def enclose_it(self, before, after):
        def enclose_textrange(c, istextcursor=False):
            tc = c
            if not istextcursor:
                tc = c.Text.createTextCursorByRange(c)
            tc.Start.setString(before)
            tc.End.setString(after)

        def whereiscursor(vc):
            tc = vc.Text.createTextCursorByRange(vc)
            if tc.isStartOfWord() or tc.isEndOfWord():
                return 'wordedge'
            tc.goLeft(1, True)
            before = tc.String
            if before:
                tc.goRight(1, False)
            tc.goRight(1, True)
            after = tc.String
            # print(set(before + after))
            if before in whitespace:
                return "isolated"
            elif set(before + after) < NONWORDCHARS:
                return "punctedge"
            else:
                return "insideword"
                
        def getquotes(quote, text):
            loc = text.CharLocale
            if loc.Language == "zxx":
                return quote, quote
            localeitems = self.localedata.getLocaleItem(loc)
            if quote == "'":
                return localeitems.quotationStart, localeitems.quotationEnd
            elif loc.Language != "fr" or loc.Country == "CH":
                return localeitems.doubleQuotationStart, localeitems.doubleQuotationEnd
            else:
                return (localeitems.doubleQuotationStart + "\u00A0",
                        "\u00A0" + localeitems.doubleQuotationEnd)

        ret = False
        doc = self.controller.Model
        undomanager = doc.UndoManager
        selections = self.controller.Selection
        vc = self.controller.ViewCursor
        doc.lockControllers()
        undomanager.enterUndoContext(f'Enclose It! : {before}...{after}.')
        try:
            if selections.Count > 1:
                for selection in selections:
                    if selection.String:
                        if before == '"' and OPTIONS["ReplaceDoubleQuote"]:
                            before, after = getquotes(before, selection)
                        elif before == "'" and OPTIONS["ReplaceSingleQuote"]:
                            before, after = getquotes(before, selection)
                        enclose_textrange(selection)
                        ret = True
            elif selections[0].String:
                for selection in selections:
                    if before == '"' and OPTIONS["ReplaceDoubleQuote"]:
                        before, after = getquotes(before, selection)
                    elif before == "'" and OPTIONS["ReplaceSingleQuote"]:
                        before, after = getquotes(before, selection)
                    enclose_textrange(selection)
                    vc.gotoRange(selection.End, False)
                    vc.goRight(len(after), False)
                    ret = True
            elif OPTIONS['closeemptycursor']:
                selection = selections[0]
                if before == '"' and OPTIONS["ReplaceDoubleQuote"]:
                    before, after = getquotes(before, selection)
                cursorposition = whereiscursor(vc)
                # print(cursorposition)
                if cursorposition == 'insideword' and before != "'":
                    tc = selection.Text.createTextCursorByRange(selection)
                    tc.gotoStartOfWord(False)
                    tc.gotoEndOfWord(True)
                    enclose_textrange(tc)
                    vc.gotoRange(tc.End, False)
                    ret = True
                elif cursorposition == 'isolated':
                    selection.End.setString(after)
                    selection.Start.setString(before)
                    vc.gotoRange(selection.Start, False)
                    vc.goRight(len(after), False)
                    ret = True
                elif cursorposition in ('wordedge', 'punctedge'):
                    pass
        except Exception:
            traceback.print_exc()
        finally:
            undomanager.leaveUndoContext()
            doc.unlockControllers()
            return ret


class Dispatcher(unohelper.Base, XDispatch):
    def __init__(self, localedata, frame, frameid, url):
        self.localedata = localedata
        self.frame = frame
        self.frameid = frameid
        self.controller = frame.Controller
        self.keyhandler = None
        self.state = False
        self.listeners = []
        self.url = url

    # XDispatch
    def dispatch(self, url, args):
        self.state = not self.state
        self.broadcast_state()
        self.registerhandler()

    def broadcast_state(self):
        ev = self.create_simple_event(self.url, self.state)
        self.status_changed(ev)

    def status_changed(self, ev):
        for listener in self.listeners:
            listener.statusChanged(ev)

    def addStatusListener(self, listener, url):
        try:
            self.listeners.index(listener)
        except ValueError:
            self.listeners.append(listener)
            self.broadcast_state()

    def removeStatusListener(self, listener, url):
        print("Enclose It! -> removeStatusListener")
        try:
            m = self.listeners.index(listener)
            self.listeners.pop(n)
            if not self.listeners:
                try:
                    FRAMES.pop(self.frameid)
                except KeyError:
                    pass
        except ValueError:
            pass

    def create_simple_event(self, url, state, enabled=True):
        return FeatureStateEvent(self, url, "", enabled, False, state)

    def registerhandler(self):
        try:
            if self.keyhandler is None:
                self.keyhandler = KeyHandler(self.localedata, self.controller)
                self.controller.addKeyHandler(self.keyhandler)
            else:
                self.controller.removeKeyHandler(self.keyhandler)
                self.keyhandler = None
        except Exception:
            traceback.print_exc()


class EncloseIt(unohelper.Base, XInitialization, XDispatchProvider, XServiceInfo):
    def __init__(self, ctx, *args):
        self.ctx = ctx
        self.sm = self.ctx.ServiceManager
        self.frame = None
        self.frameid = None
        self.load_arguments()

    # XInitialization
    def initialize(self, args):
        print("Enclose It! -> XInitialization")
        if len(args) > 0:
            self.frame = args[0]
            self.frameid = self.frame.Controller.Model.StringValue

    # XDispatchProvider
    def queryDispatch(self, url, name, flag):
        dispatcher = None
        if url.Main == "ooo.jmz.enclose_it:Enclose":
            try:
                dispatcher = FRAMES[self.frameid]
            except KeyError:
                localedata = self.create("com.sun.star.i18n.LocaleData")
                dispatcher = Dispatcher(localedata, self.frame, self.frameid, url)
                FRAMES[self.frameid] = dispatcher
                if OPTIONS['autostart']:
                    dispatcher.dispatch(url, ())
            except Exception as e:
                traceback.print_exc()
        return dispatcher

    def queryDispatches(self, descs):
        pass

    # XServiceInfo
    def supportsService(self, name):
        return (name == "com.sun.star.frame.ProtocolHandler")

    def getImplementationName(self):
        return IMPL_NAME

    def getSupportedServiceNames(self):
        return ("com.sun.star.frame.ProtocolHandler",)

    # private
    def create(self, service):
        '''Instanciate UNO services'''
        return self.sm.createInstance(service)

    def load_arguments(self):
        cfg = self.create('com.sun.star.configuration.ConfigurationProvider')
        global cfg_access_registry
        if not cfg_access_registry:
            prop = PropertyValue('nodepath', 0, '/ooo.jmz.enclose_it.Registry/Options', 0)
            cfg_access_registry = cfg.createInstanceWithArguments('com.sun.star.configuration.ConfigurationAccess', (prop,))
            properties = cfg_access_registry.ElementNames
            values = cfg_access_registry.getPropertyValues(properties)
            OPTIONS.update(zip(properties, values))
            cfg_access_registry.addChangesListener(ChangesListener(properties))
            # update PAIRS characters
            updatepairs()
        global cfg_access_autocorrect
        if not cfg_access_autocorrect:
            pass
            prop = PropertyValue('nodepath', 0, '/org.openoffice.Office.Common/AutoCorrect', 0)
            cfg_access_autocorrect = cfg.createInstanceWithArguments('com.sun.star.configuration.ConfigurationAccess', (prop,))
            properties = ("ReplaceSingleQuote", "ReplaceDoubleQuote")
            values = cfg_access_autocorrect.getPropertyValues(properties)
            OPTIONS.update(zip(properties, values))
            cfg_access_autocorrect.addChangesListener(ChangesListener(properties))


g_ImplementationHelper = unohelper.ImplementationHelper()
g_ImplementationHelper.addImplementation(EncloseIt, IMPL_NAME, ("com.sun.star.frame.ProtocolHandler",),)
