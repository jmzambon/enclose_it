# *Enclose It!*

###### [[Français](#english)]

<details><summary>Demonstration (animated gif)</summary>
![peek.gif](peek.gif)
</details>

### What is it all about?
*Enclose It!* is a small extension for LibreOffice Writer.

Once installed ([infos](https://help.libreoffice.org/7.0/en-GB/text/shared/01/packagemanager.html#hd_id3895382)), it allows the user to enable a direct input mode of some paired punctuations (parentheses, square brackets, curly brackets and single or double quotes): select a word or a portion of text, type the opening symbol and the entire selection will automatically be enclosed by the corresponding opening and closing characters.

### For which users?
*Enclose It!* has been designed to ease the work of LibreOffice Writer users who frequently have to revise texts already written. Adding parentheses or quotes to existing text is much quicker with the help of the direct mode added by *Enclose It!* .

Other people who are used to this type of input (notably through the use of specialized text editors) should also appreciate this alternative behaviour while typing normal text.

### How it works
Once installed, the *Enclose It!* extension adds the [ (...) Enclose It! ] item to the standard Writer toolbar, just to the right of the [ ¶ Formatting marks ] item.

To activate the alternative input mode added by *Enclose It!*, simply click this toggle button. Click again to disable the alternative input mode.

By default, the extension only supports parentheses ( ( ) ), square brackets ( [ ] ), curly brackets ( { } ) and single ( ' ' ) or double ( " " ) quotes. However, it is possible to add custom pairs in the advanced options (for example < and >).

### Features
- Works with single or multiple selection.
- Works without selection when the cursor is inside a word (the whole word is enclosed).
- Works without selection when the cursor is isolated. The cursor is then moved between the newly inserted punctuation marks to continue typing from inside (this behaviour can be disabled in the advanced options).
- Complies with the linguistic and autocorrection options for single and double quotes.
- Allows personal punctuation pairs to be added via the advanced options.
- Allows the new input mode to be set as default via the advanced options.

---

###### [[English](#fran%C3%A7ais)]

<details><summary>Démonstration (gif animé)</summary>
![peek_fr.gif](peek_fr.gif)
</details>

### De quoi s'agit-il ?
*Enclose It!* est une petite extension pour LibreOffice Writer.

Une foix installée ([infos](https://help.libreoffice.org/7.0/fr/text/shared/01/packagemanager.html#hd_id3895382)), elle permet d'activer un mode de saise directe de certaines ponctuations fonctionnant par paires (parenthèses, crochets, accollades et guillemets simples ou doubles) : sélectionnez un mot ou une portion de texte, tapez le symbole ouvrant et la sélection entière sera automatiquement encadrée par les caractères ouvrant et fermant correspondants.

### Pour quels utilisateurs ?
*Enclose It!* a été conçu pour faciliter le travail des utilisateurs de LibreOffice Writer fréquemment amenés à réviser des textes déjà écrits. Ajouter des parenthèses ou des guillemets à un texte existant est en effet plus rapide avec l'aide du mode direct ajouté par *Enclose It!* .

D'autres, qui seraient habitués à ce type de fonctionnement (notamment via l'usage d'éditeurs de texte spécialisés), apprécieront sans doute également ce nouveau comportement lors d'une saisie normale de texte.

### Principe de fonctionnement
Une fois installée, l'extension *Enclose It!* ajoute l'élément [ (...) Enclose It! ] à la barre d'outils standard de Writer, juste à droite de l'élément [ ¶ marques de formatage ].

Pour activer le mode de saisie alternatif ajouté par *Enclose It!*, il suffit de cliquer sur ce bouton. De même, un nouveau clic permet d'annuler le mode de saisie alternatif.

Par défaut, l'extension ne prend en compte que les parenthèses ( ( ) ), les crochets ( [ ] ), les accolades ( { } ) et les guillemets simples ( ' ' ) ou doubles ( " " ). Il est toutefois possible d'ajouter d'autres paires dans les options avancées (par exemple < et >).

### Caractéristiques
- Fonctionne avec une sélection simple ou multiple.
- Fonctionne sans sélection lorsque le curseur se trouve à l'intérieur d'un mot (le mot entier est alors encadré).
- Fonctionne sans sélection lorsque le curseur est isolé. Le curseur se place alors entre les deux ponctuations insérées pour poursuivre la saisie (ce comportement est désactivable dans les options avancées).
- Respecte les options linguistiques et d'autocorrection pour ce qui concerne les guillemets simples et doubles.
- Permet l'ajout de paires de ponctuations personnelles via les options avancées.
- Permet de définir le nouveau mode de saisie comme mode par défaut via les options avancées.
